from django.contrib import admin
from game.models import Game, GameSquare, GameLog


admin.site.register(Game)
admin.site.register(GameSquare)
admin.site.register(GameLog)
